Clerk is an Identity and Access Management Framework
====================================================

Clerk is a straightforward framework for user identification, authentication and access control.

The project has support for user groups and typical account validation or password reset via email.

