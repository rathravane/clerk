/*-
 * #%L
 * Rathravane Clerk
 * %%
 * Copyright (C) 2006 - 2016 Rathravane LLC
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package com.rathravane.clerk.app.iam;

import junit.framework.TestCase;

import org.junit.Test;

import com.rathravane.clerk.IamServiceManager;
import com.rathravane.clerk.credentials.UsernamePasswordCredential;
import com.rathravane.clerk.exceptions.IamBadRequestException;
import com.rathravane.clerk.exceptions.IamGroupExists;
import com.rathravane.clerk.exceptions.IamIdentityDoesNotExist;
import com.rathravane.clerk.exceptions.IamIdentityExists;
import com.rathravane.clerk.exceptions.IamSvcException;
import com.rathravane.clerk.identity.Group;
import com.rathravane.clerk.identity.Identity;
import com.rathravane.clerk.identity.IdentityManager;
import com.rathravane.till.time.clock;

public class IamTest extends TestCase
{
	@Test
	public void testSignonWithPasswordOk () throws IamSvcException, IamIdentityDoesNotExist, IamIdentityExists, IamGroupExists
	{
		final IamServiceManager<?,?> am = BasicTest.makeSimpleDb ();

		final Group group = am.getAccessManager().createGroup ( "a1" );
		assertEquals ( "a1", group.getName () );

		final IdentityManager<?> im = am.getIdentityManager ();
		final Identity newUser = im.createUser ( "u1@example.com" );

		newUser.setPassword ( "pwd" );
		newUser.enable ( true );

		assertNotNull ( im.authenticate ( new UsernamePasswordCredential ( "u1@example.com", "pwd" ) ) );
		assertNull ( im.authenticate ( new UsernamePasswordCredential ( "u1@example.com", "wrongpwd" ) ) );

		newUser.enable ( false );
		assertNull ( im.authenticate ( new UsernamePasswordCredential ( "u1@example.com", "pwd" ) ) );
	}

	@Test
	public void testSignonWithBadPassword () throws IamSvcException, IamIdentityDoesNotExist, IamIdentityExists, IamGroupExists
	{
		final IamServiceManager<?,?> am = BasicTest.makeSimpleDb ();

		final Group group = am.getAccessManager().createGroup ( "a1" );
		assertEquals ( "a1", group.getName () );

		final IdentityManager<?> im = am.getIdentityManager ();
		final Identity newUser = im.createUser ( "u1@example.com" );

		newUser.setPassword ( "pwd" );
		newUser.enable ( true );

		assertNull ( im.authenticate ( new UsernamePasswordCredential ( "u1@example.com", "wrongpwd" ) ) );
	}

	@Test
	public void testSignonWithNoPasswordSet () throws IamIdentityDoesNotExist, IamSvcException, IamIdentityExists, IamGroupExists 
	{
		final IamServiceManager<?,?> am = BasicTest.makeSimpleDb ();

		final Group group = am.getAccessManager().createGroup ( "a1" );
		assertEquals ( "a1", group.getName () );

		final IdentityManager<?> im = am.getIdentityManager ();
		final Identity newUser = im.createUser ( "u1@example.com" );
		newUser.enable ( true );
		assertNull ( im.authenticate ( new UsernamePasswordCredential ( "u1@example.com", "wrongpwd" ) ) );
	}

	@Test
	public void testSignonWithUserDisabled () throws IamSvcException, IamIdentityDoesNotExist, IamIdentityExists, IamGroupExists 
	{
		final IamServiceManager<?,?> am = BasicTest.makeSimpleDb ();

		final Group group = am.getAccessManager().createGroup ( "a1" );
		assertEquals ( "a1", group.getName () );

		final IdentityManager<?> im = am.getIdentityManager ();
		final Identity newUser = im.createUser ( "u1@example.com" );

		newUser.setPassword ( "pwd" );
		newUser.enable ( false );

		assertNull ( im.authenticate ( new UsernamePasswordCredential ( "u1@example.com", "pwd" ) ) );
	}

	@Test
	public void testPasswordReset () throws IamSvcException, IamBadRequestException
	{
		final IamServiceManager<?,?> am = BasicTest.makeSimpleDb ();

		final Group group = am.getAccessManager().createGroup ( "a1" );
		assertEquals ( "a1", group.getName () );

		final IdentityManager<?> im = am.getIdentityManager ();
		final Identity newUser = im.createUser ( "u1@example.com" );
		newUser.setPassword ( "pwd" );
		newUser.enable ( true );

		final String token = newUser.requestPasswordReset ( 60 * 60 * 24, "" + clock.now () );

		// password should not change yet
		assertNotNull ( im.authenticate ( new UsernamePasswordCredential ( "u1@example.com", "pwd" ) ) );

		im.completePasswordReset ( token, "new" );

		// old password fails
		assertNull ( im.authenticate ( new UsernamePasswordCredential ( "u1@example.com", "pwd" ) ) );

		// new password works
		assertNotNull ( im.authenticate ( new UsernamePasswordCredential ( "u1@example.com", "new" ) ) );
	}

//	@Test
//	public void testPasswordResetFabricatedToken () throws DrumlinAccountsException
//	{
//		final DrumlinAccountPersistence<user,account> dap = getModel ();
//		final DrumlinAccountMgr<user,account> am = new DrumlinAccountMgr<user,account> ( dap );
//
//		am.createAccount ( "a1" );
//		am.enableAccount ( "a1", true );
//		assertTrue ( am.accountExists ( "a1" ) );
//
//		am.createUser ( "a1", "u1", "u1@a1.example.com" );
//		am.enableUser ( "u1", true );
//		am.setPassword ( "u1", "pwd" );
//		assertTrue ( am.userExists ( "u1" ) );
//
//		user u = am.authenticateUserWithPassword ( "u1", "pwd" );
//		assertNotNull ( u );
//
//		am.completePasswordReset ( "bogus token", "new" );
//
//		// old password still works
//		u = am.authenticateUserWithPassword ( "u1", "pwd" );
//		assertNotNull ( u );
//
//		// new password fails
//		u = am.authenticateUserWithPassword ( "u1", "new" );
//		assertNull ( u );
//	}
//
//	@Test
//	public void testPasswordResetExpiredToken () throws DrumlinAccountsException
//	{
//		// setup a test clock
//		final clock.testClock tc = clock.useNewTestClock ();
//		
//		final DrumlinAccountPersistence<user,account> dap = getModel ();
//		final DrumlinAccountMgr<user,account> am = new DrumlinAccountMgr<user,account> ( dap );
//
//		am.createAccount ( "a1" );
//		am.enableAccount ( "a1", true );
//		assertTrue ( am.accountExists ( "a1" ) );
//
//		am.createUser ( "a1", "u1", "u1@a1.example.com" );
//		am.setPassword ( "u1", "pwd" );
//		am.enableUser ( "u1", true );
//		assertTrue ( am.userExists ( "u1" ) );
//
//		user u = am.authenticateUserWithPassword ( "u1", "pwd" );
//		assertNotNull ( u );
//
//		// token that expires in 8 hrs
//		final String token = am.requestPasswordReset ( "u1", 8 * 60 * 60, "" + clock.now () );
//
//		// advance the clock one day
//		tc.add ( 24, TimeUnit.HOURS );
//
//		// call the password reset
//		final boolean reset = ( null != am.completePasswordReset ( token, "new" ) );
//		assertFalse ( reset );
//
//		// old password still works
//		u = am.authenticateUserWithPassword ( "u1", "pwd" );
//		assertNotNull ( u );
//
//		// new password fails
//		u = am.authenticateUserWithPassword ( "u1", "new" );
//		assertNull ( u );
//	}
}
