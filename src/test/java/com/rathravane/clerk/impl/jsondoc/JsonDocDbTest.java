/*-
 * #%L
 * Rathravane Clerk
 * %%
 * Copyright (C) 2006 - 2016 Rathravane LLC
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package com.rathravane.clerk.impl.jsondoc;

import junit.framework.TestCase;

import org.junit.Test;

import com.rathravane.clerk.credentials.UsernamePasswordCredential;
import com.rathravane.clerk.exceptions.IamBadRequestException;
import com.rathravane.clerk.exceptions.IamIdentityDoesNotExist;
import com.rathravane.clerk.exceptions.IamSvcException;
import com.rathravane.clerk.identity.ApiKey;
import com.rathravane.clerk.impl.common.CommonJsonIdentity;
import com.rathravane.till.time.clock;

public class JsonDocDbTest extends TestCase
{
	@Test
	public void testApiKeyCreateOnNullUser () throws IamIdentityDoesNotExist, IamSvcException
	{
		try
		{
			final JsonDocDb db = new JsonDocDb ();
			db.createApiKey ( null );
			fail ( "can't create api key on null user" );
		}
		catch ( IamBadRequestException x )
		{
			// good
		}
	}

	@Test
	public void testApiKeyCreate () throws IamSvcException, IamBadRequestException
	{
		final JsonDocDb db = new JsonDocDb ();
		
		final CommonJsonIdentity i = db.createUser ( "test" );
		assertNotNull ( i );

		final ApiKey key = db.createApiKey ( i.getId () );

		assertNotNull ( key );
		assertNotNull ( key.getKey () );
		assertNotNull ( key.getSecret () );
	}

	@Test
	public void testPasswordReset () throws IamSvcException, IamBadRequestException
	{
		final clock.testClock tc = clock.useNewTestClock ();

		final JsonDocDb db = new JsonDocDb ();

		final CommonJsonIdentity i = db.createUser ( "test" );
		assertNotNull ( i );

		tc.add ( 100 );

		String resetTag = i.requestPasswordReset ( 10, "nonce" );
		assertTrue ( db.completePasswordReset ( resetTag, "foobar" ) );

		assertNotNull ( db.authenticate ( new UsernamePasswordCredential ( "test", "foobar" ) ) );

		resetTag = i.requestPasswordReset ( 10, "nonce" );
		tc.add ( 50000 );
		assertFalse ( db.completePasswordReset ( resetTag, "foobar" ) );
	}
}
