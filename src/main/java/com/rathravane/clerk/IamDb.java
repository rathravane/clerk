/*-
 * #%L
 * Rathravane Clerk
 * %%
 * Copyright (C) 2006 - 2016 Rathravane LLC
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package com.rathravane.clerk;

import com.rathravane.clerk.access.AccessManager;
import com.rathravane.clerk.identity.Group;
import com.rathravane.clerk.identity.Identity;
import com.rathravane.clerk.identity.IdentityManager;
import com.rathravane.clerk.tags.TagManager;

public interface IamDb<I extends Identity,G extends Group> extends
	IdentityManager<I>, AccessManager<G>, TagManager
{

}
