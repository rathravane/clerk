/*-
 * #%L
 * Rathravane Clerk
 * %%
 * Copyright (C) 2006 - 2016 Rathravane LLC
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package com.rathravane.clerk;

import com.rathravane.clerk.access.AccessDb;
import com.rathravane.clerk.exceptions.IamSvcException;
import com.rathravane.clerk.identity.Group;
import com.rathravane.clerk.identity.Identity;
import com.rathravane.clerk.identity.IdentityDb;

/**
 * Identity and access lookup interface, planned for "lookups" rather than
 * the management of identity, groups, access, etc.
 * 
 * @author peter
 *
 */
public interface IamService<I extends Identity, G extends Group>
{
	/**
	 * Get the identity database
	 * @return the identity database
	 * @throws IamSvcException
	 */
	IdentityDb<I> getIdentityDb () throws IamSvcException;

	/**
	 * Get the access database
	 * @return the access database
	 * @throws IamSvcException
	 */
	AccessDb<G> getAccessDb () throws IamSvcException;
}
