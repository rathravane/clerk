/*-
 * #%L
 * Rathravane Clerk
 * %%
 * Copyright (C) 2006 - 2016 Rathravane LLC
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package com.rathravane.clerk.impl.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rathravane.clerk.credentials.ApiKeyCredential;
import com.rathravane.till.nv.rrNvReadable;

/**
 * Web systems implementing RESTful APIs with API keys can use this helper class to
 * read API key authentication info from the inbound request. This class works well
 * with the Drumlin request context, but in order to keep the project dependencies 
 * simple, the request is abstracted with the HeaderReader interface.
 * 
 * @author peter
 *
 */
public class ApiKeyAuthHelper
{
	public static final String kSetting_AuthLineHeader = "api.headers.auth";
	public static final String kSetting_DateLineHeader = "api.headers.date";
	public static final String kSetting_MagicLineHeader = "api.headers.magic";

	public static final String kDefault_AuthLineHeader = "X-Auth";
	public static final String kDefault_DateLineHeader = "X-Date";
	public static final String kDefault_MagicLineHeader = "X-Magic";

	/**
	 * Build an ApiKeyCredential from an inbound HTTP header.
	 * 
	 * @param settings 
	 * @param hr
	 * @param serviceName
	 * @return an API key credential, or null if the header is malformed, etc.
	 */
	public static ApiKeyCredential readApiKeyCredential ( rrNvReadable settings, HeaderReader hr, String serviceName )
	{
		final String authHeader = settings.getString ( kSetting_AuthLineHeader, kDefault_AuthLineHeader );
		final String authLine = hr.getFirstHeader ( authHeader );
		if ( authLine == null )
		{
			authLog ( "No " + authHeader + " header" );
			return null;
		}

		final String signedContent = SignedContentReader.getSignedContent ( 
			hr.getFirstHeader ( "Date" ),
			hr.getFirstHeader ( settings.getString ( kSetting_DateLineHeader, kDefault_DateLineHeader )), 
			hr.getFirstHeader ( settings.getString ( kSetting_MagicLineHeader, kDefault_MagicLineHeader )),
			serviceName );
		if ( signedContent == null )
		{
			return null;
		}
		authLog ( "authLine [" + authLine + "]" );

		final int colon = authLine.indexOf ( ':' );
		if ( colon == -1 )
		{
			authLog ( "" + authHeader + " is malformed" );
			return null;
		}

		final String apiKey = authLine.substring ( 0, colon );
		final String signature = authLine.substring ( colon + 1 );
		authLog ( "key: " + apiKey + "; signature: " + signature );

		return new ApiKeyCredential ( apiKey, signedContent, signature );
	}

	private static final Logger log = LoggerFactory.getLogger ( ApiKeyAuthHelper.class );
	private static final boolean skAuthLogging = true;
	private static void authLog ( String msg )
	{
		if ( skAuthLogging )
		{
			log.info ( msg );
		}
		else
		{
			log.debug ( msg );
		}
	}
}
