/*-
 * #%L
 * Rathravane Clerk
 * %%
 * Copyright (C) 2006 - 2016 Rathravane LLC
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package com.rathravane.clerk.impl.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rathravane.clerk.credentials.UsernamePasswordCredential;
import com.rathravane.till.data.rrConvertor;
import com.rathravane.till.nv.rrNvReadable;

/**
 * Web systems implementing RESTful APIs with basic auth can use this helper class to
 * credentials from the inbound request. This class works well
 * with the Drumlin request context, but in order to keep the project dependencies 
 * simple, the request is abstracted with the HeaderReader interface.
 * 
 * @author peter
 *
 */
public class BasicAuthHelper
{
	public static final String kSetting_AuthHeader = "Authorization";

	/**
	 * Build an UsernamePasswordCredential from an inbound HTTP header.
	 * 
	 * @param settings 
	 * @param hr
	 * @return username password credential or null if the header is malformed, etc.
	 */
	public static UsernamePasswordCredential readUsernamePasswordCredential ( rrNvReadable settings, HeaderReader hr )
	{
		final String authLine = hr.getFirstHeader ( kSetting_AuthHeader );
		if ( authLine == null )
		{
			authLog ( "No " + kSetting_AuthHeader + " header" );
			return null;
		}

		authLog ( "authLine [" + authLine + "]" );

		final String[] parts = authLine.split ( " " );
		if ( parts.length != 2 || !parts[0].equals ( "Basic" ) )
		{
			authLog ( kSetting_AuthHeader + " value is illegal" );
			return null;
		}

		final byte[] decoded = rrConvertor.base64Decode ( parts[1] );
		final String creds = new String ( decoded );	// FIXME: charset concerns here?

		final int colon = creds.indexOf ( ':' );
		if ( colon == -1 )
		{
			authLog ( kSetting_AuthHeader + " is malformed" );
			return null;
		}

		final String username = creds.substring ( 0, colon );
		final String password = creds.substring ( colon + 1 );
		authLog ( "username: " + username );

		return new UsernamePasswordCredential ( username, password );
	}

	private static final Logger log = LoggerFactory.getLogger ( BasicAuthHelper.class );
	private static final boolean skAuthLogging = true;
	private static void authLog ( String msg )
	{
		if ( skAuthLogging )
		{
			log.info ( msg );
		}
		else
		{
			log.debug ( msg );
		}
	}
}
