/*-
 * #%L
 * Rathravane Clerk
 * %%
 * Copyright (C) 2006 - 2016 Rathravane LLC
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
/*
 *	Copyright 2006-2012, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.clerk.impl.s3;

import java.util.Hashtable;
import java.util.LinkedList;

class lruCache <K extends Object, T extends Object>
{
	public interface expulsionListener<K,T>
	{
		void onExpelled ( K key, T value );
	}
	
	public lruCache ( long maxSize )
	{
		fEntries = new Hashtable<K,entry> ();
		fMruList = new LinkedList<K> ();

		setMaxSize ( maxSize );
	}

	public T get ( K key )
	{
		return get ( key, -1 );
	}

	public T get ( K key, long maxAgeMs )
	{
		return lookup ( key, maxAgeMs );
	}

	public synchronized T lookup ( K key )
	{
		return lookup ( key, -1 );
	}

	/**
	 * Lookup the value for a key. If a max age is specified (>-1) and the
	 * entry exists but is older than the max age, the entry is removed from
	 * the cache.
	 * 
	 * @param key
	 * @param maxAgeMs
	 * @return
	 */
	public synchronized T lookup ( K key, long maxAgeMs )
	{
		T t = null;
		entry e = fEntries.get ( key );
		if ( e != null )
		{
			if ( maxAgeMs > -1 )
			{
				final long now = System.currentTimeMillis ();
				final long age = now - e.initialInsertMs;
				if ( age > maxAgeMs )
				{
					drop ( key );
					return null;
				}
			}
			
			t = e.value;
			noteUse ( key );
		}
		return t;
	}

	public void put ( K key, T object )
	{
		store ( key, object );
	}

	public void store ( K key, T object )
	{
		store ( key, object, null );
	}
	
	public synchronized void store ( K key, T object, expulsionListener<K,T> el )
	{
		ensureCapacity ();
		if ( fEntries.size() < fMaxSize )	// max size can be 0
		{
			final entry e = new entry ();
			e.value = object;
			e.expulsion = el;
			e.initialInsertMs = System.currentTimeMillis ();
			fEntries.put ( key, e );
			noteUse ( key );
		}
	}

	public synchronized void drop ( K key )
	{
		fEntries.remove ( key );
		fMruList.remove ( key );
	}

	public synchronized long maxSize ()
	{
		return fMaxSize;
	}

	public synchronized void setMaxSize ( long size )
	{
		if ( size < 0 )
		{
			size = 0;
		}
		fMaxSize = size;
		ensureCapacity ();
	}

	public synchronized void clear ()
	{
		fEntries.clear ();
		fMruList.clear ();
	}

	private long fMaxSize;
	private final Hashtable<K,entry> fEntries;
	private final LinkedList<K> fMruList;	// first item is MRU

	private void noteUse ( K key )
	{
		fMruList.remove ( key );	// FIXME: this is likely O(n)
		fMruList.addFirst ( key );
	}

	private class entry
	{
		T value;
		expulsionListener<K,T> expulsion;
		long initialInsertMs;
	}

	private void ensureCapacity ()
	{
		while ( fEntries.size() >= fMaxSize && fEntries.size() != 0 )
		{
			final K key = fMruList.removeLast ();
			final entry e = fEntries.remove ( key );
			if ( e.expulsion != null )
			{
				e.expulsion.onExpelled ( key, e.value );
			}
		}
	}
}
