/*-
 * #%L
 * Rathravane Clerk
 * %%
 * Copyright (C) 2006 - 2016 Rathravane LLC
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package com.rathravane.clerk.identity;

import com.rathravane.clerk.credentials.ApiKeyCredential;
import com.rathravane.clerk.credentials.UsernamePasswordCredential;
import com.rathravane.clerk.exceptions.IamSvcException;

/**
 * An identity database, mainly for authenticating users.
 * @author peter
 *
 */
public interface IdentityDb<I extends Identity>
{
	/**
	 * Authenticate with a username and password
	 * @param upc
	 * @return an authenticated identity or null
	 */
	I authenticate ( UsernamePasswordCredential upc ) throws IamSvcException;

	/**
	 * Authenticate with an API key and signature
	 * @param akc
	 * @return an authenticated identity or null
	 */
	I authenticate ( ApiKeyCredential akc ) throws IamSvcException;
}
